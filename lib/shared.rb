module Lambic
  class RESTError < StandardError
    attr_reader :response
    def initialize ( response: )
      @response = response
      @message = response.reason
    end

    def url
      @response.header.request_uri.to_s
    end

    def status_code
      @response.status
    end
  end
end
