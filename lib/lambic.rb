require_relative "./shared"
require "require_all"
require "httpclient"

require_rel "storage"

# Binding framework for Oracle Cloud Services REST APIs
module Lambic
  VERSION = "1.0.0"
end
