module Lambic
  module Storage

    # Provides "object" operations for the storage service.
    # Named +StorageObject+ to avoid confusion with Ruby root class +Object+
    # (it seemed worth the extra typing)!
    class StorageObject
      attr_reader :name, :container_name

      # Metadata header prefix for custom metadata
      META_PREFIX = "X-Object-Meta-"

      # Creates a new instance of +Lambic::Storage::StorageObject+
      #
      # Example:
      #   sh = Handler.new domain: "gboracle12464", username: "ann.onymous@oracle.com", password: "Pa55w0rd"
      #   so = StorageObject.new handler: sh, container_name: "ruby-horse", name: "vw-fire-engine.jpg"
      def initialize ( handler:, container_name:, name: )
        @handler = handler
        @container_name = container_name
        @name = name
      end

      # Returns +true+ if this +StorageObject+ exists in the storage service.
      def exists?
        parent.exists? ? parent.has?(object_name: name) : false
      end

      # Creates the +StorageObject+ in the storage service and uploads the
      # contents of +filename+ to it.
      def create ( filename: )
        handler.put url: url, body: File.open(filename) { |io| io.read }
      end

      # Deletes this +StorageObject+
      def delete
        handler.delete url: url
      end

      # If this +StorageObject+ exists, its contents are copied to
      # +container_name+/+object_name+.
      # If +object_name+ is not specified, then the +name+ of this
      # +StorageObject+ is used.
      def copy_to ( container_name:, object_name: name )
        if exists?
          handler.put(url: url, headers: {"Destination" => "/#{container_name}/#{object_name}"})
        end
      end

      # If this +StorageObject+ exists, it's contents are downloaded to
      # +filename+
      def download_to ( filename: )
        handler.download(url: url, filename: filename) if exists?
      end

      # Set +metadata_headers+ (supplied as a +Hash+) for this +StorageObject+.
      # Use the +Lambic::Storage::StorageObject::META_PREFIX+ constant.
      def set_metadata ( metadata_headers: )
        handler.post url: url, headers: metadata_headers
      end

      # Returns the metadata for this +StorageObject+ as a +Hash+.
      # Use the +Lambic::Storage::StorageObject::META_PREFIX+ constant.
      def get_metadata
        handler.head(url: url).headers
      end

      # Restore this +StorageObject+ from archive.
      # Returns the tracking URL
      def restore_from_archive
        restore_url = url(tgt_api: "v0") + "?restore"
        handler.post(url: restore_url).headers["X-Archive-Restore-Tracking"]
      end

      # Tracks the progress of the restore job.
      # track_url - the tracking URL returned when the restore job was
      # created.
      def track_restore ( track_url: )
        handler.get url: track_url
      end

      private def url tgt_api: "v1"
        handler.api_url(api: tgt_api) + "/#{container_name}/#{name}"
      end

      private def parent
        @parent ||= Container.new(handler: handler, name: container_name)
      end

      private def handler
        @handler
      end
    end
  end
end
