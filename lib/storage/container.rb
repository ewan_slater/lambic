module Lambic
  module Storage
    # Provides +Container+ operations for the storage service.
    class Container
      # Metadata header prefix for quota in bytes
      QUOTA_BYTES = "X-Container-Meta-Quota-Bytes"
      # Metadata header prefix for quota in number of objects
      QUOTA_OBJECTS = "X-Container-Meta-Quota-Count"
      # Metadata header prefix for read Access Control List (ACL)
      READ_ACL = "X-Container-Read"
      # Metadata header prefix for read Access Control List (ACL)
      WRITE_ACL = "X-Container-Write"
      # Metadata header prefix for storage class
      STORAGE_CLASS = "X-Storage-Class"
      # Metadata header value for storage class +archive+
      STORAGE_CLASS_ARCHIVE = "Archive"
      # Metadata header prefix for custom metadata
      META_PREFIX = "X-Container-Meta-"

      attr_reader :name

      # Creates a new instance of +Lambic::Storage::Container+
      #
      # Example:
      #   sh = Handler.new domain: "gboracle12464", username: "ann.onymous@oracle.com", password: "Pa55w0rd"
      #   sc = Container.new handler: sh, name: "ruby-horse"
      def initialize ( handler:, name: )
        @handler = handler
        @name = name
      end

      # Creates this +Container+ unless it already exists in the storage
      # service.
      def create
        handler.put url: url unless exists?
      end

      # Creates this +Container+ with +STORAGE_CLASS_ARCHIVE+ unless it already
      # exists in the storage service.
      def create_as_archive
        handler.put url: url, headers: {STORAGE_CLASS => STORAGE_CLASS_ARCHIVE} unless exists?
      end

      # Deletes the +Container+ if it is empty.
      def delete
        handler.delete url: url if empty?
      end

      # Deletes this +Container+ and it's contents.
      def delete!
        delete_objects
        delete
      end

      # Deletes the contents of this +Container+.
      def delete_objects
        list_objects.each do | obj_name |
          handler.delete(url: url + "/" + obj_name)
        end
      end

      # Returns +true+ if this +Container+ is empty.
      def empty?
        list_objects.empty?
      end


      # Returns +true+ if this +Container+ exists in the storage service.
      def exists?
        parent.has? container_name: name
      end

      # Set the +metadata_headers+ (supplied as a Hash) for this +Container+.
      # Commonly used prefixes are listed in the constants for +Container+.
      def set_metadata ( metadata_headers: )
        handler.post url: url, headers: metadata_headers
      end

      # Returns the metadata for this +Container+ as a +Hash+.
      # Commonly used prefixes are listed in the constants for +Container+.
      def get_metadata
        handler.head(url: url).headers
      end

      # Returns an +Array+ of names of objects stored in this +Container+.
      # Results are filtered if +filter_by+ and +filter_value+ are specified.
      def list_objects ( filter_by: nil, filter_value: nil )
        query = filter_by.nil? ? "" : "?#{filter_by}=#{filter_value}"
        handler.get(url: url + query).body.split(/\n/)
      end

      # Returns true if +object_name+ is stored in this +Container+
      def has? ( object_name: )
        list_objects.include? object_name
      end

      # If +object_name+ is stored in this container it is returned
      # as an instance of +StorageObject+.
      # Otherwise returns nil.
      def fetch ( object_name: )
        if has? object_name: object_name
          StorageObject.new(handler: handler, container_name: name,\
            name: object_name)
        end
      end

      # Returns the contents of this container as an +Array+ of +StorageObject+.
      def fetch_all
        list_objects.map { |e| fetch object_name: e }
      end

      private def parent
        @parent ||= Account.new handler: handler
      end

      private def url
        handler.api_url(api: "v1") + "/#{name}"
      end

      private def handler
        @handler
      end
    end
  end
end
