module Lambic
  module Storage
    # Handles authentication, token expiry and sends HTTP requests to the
    # storage service.
    class Handler
      attr_reader :domain, :proxy, :debug_file

      # Creates a new instance of +Lambic::Storage::Handler+.
      #
      # If +debug_file+ specified http log is written to the file named.
      # Specify +proxy+ if required to access the storage service.
      #
      # Example:
      #   sh = Handler.new domain: "gboracle12464", username: "ann.onymous@oracle.com", password: "Pa55w0rd"
      def initialize domain:, username:, password:, proxy: nil, debug_file: nil
        @domain = domain
        @username = username
        @password = password
        @client = HTTPClient.new proxy
        @client.debug_dev= File.new debug_file, "w" if debug_file
      end

      # Returns the URL for the specified API version for this +Handler+ domain.
      def api_url ( api: )
        domain_url + "/" + api + "/" + storage_domain
      end

      # Sends GET to +url+ with any (optional) +headers+.
      # Authentication is handled transparently.
      def get ( url:, headers: {} )
        request { client.get url, :header => headers.merge(auth_header) }
      end

      # Downloads contents of +url+ to +filename+.
      # Authentication is handled transparently.
      def download ( url:, filename: )
        f = File.new filename, "w"
        request { client.get(url, :header => auth_header,
          :follow_redirect => true) { |chunk| f.write chunk } }
        f.close
      end

      # Sends HEAD to +url+ with any (optional) +headers+.
      # Authentication is handled transparently.
      def head ( url:, headers: {} )
        request { client.head url, :header => headers.merge(auth_header) }
      end

      # Sends PUT to +url+ with (optional) +body+ and any (optional) +headers+.
      # Authentication is handled transparently.
      def put ( url:, body: nil, headers: {} )
        request { client.put url, :body => body,
          :header => headers.merge(auth_header) }
      end

      # Sends DELETE to +url+ with any (optional) +headers+.
      # Authentication is handled transparently.
      def delete ( url:, headers: {} )
        request { client.delete url, :header => headers.merge(auth_header) }
      end

      # Sends POST to +url+ with (optional) +body+ and any (optional) +headers+.
      # Authentication is handled transparently.
      def post ( url:, body: nil, headers: {} )
        request { client.post url, :body => body,
          :header => headers.merge(auth_header) }
      end

      def clear_auth_header
        @auth_header = nil
      end

      private def client
        @client
      end

      private def password
        @password
      end

      private def username
        @username
      end

      private def auth_header
        @auth_header ||= { "X-Auth-Token" => get_auth_token }
      end

      private def domain_url
        "https://" + domain + ".storage.oraclecloud.com"
      end

      private def auth_url
        domain_url + "/auth/v1.0"
      end

      private def storage_domain
        "Storage-#{domain}"
      end

      private def get_auth_token
        headers = { "X-Storage-User" => "#{storage_domain}:#{username}",
         "X-Storage-Pass" => password }
        r = client.get auth_url, :header => headers
        raise RESTError.new(response: r) unless r.ok?
        r.headers["X-Auth-Token"]
      end

      private def request
        if ((r = yield).status_code == 401)
          clear_auth_header
          r = yield
        end
        r.ok? ? r : raise(RESTError.new(response: r))
      end
    end
  end
end
