module Lambic
  module Storage
    # Provides +Account+ operations for the storage service.
    class Account
      # Metadata header prefix for custom metadata
      META_PREFIX = "X-Account-Meta-"

      # Creates a new instance of +Lambic::Storage::Account+
      #
      # Example:
      #   sh = Handler.new domain: "gboracle12464", username: "ann.onymous@oracle.com", password: "Pa55w0rd"
      #   sa = Account.new handler: sh
      def initialize ( handler: )
        @handler = handler
      end

      # Set the +metadata_headers+ (supplied as a +Hash+) for this +Account+.
      # Use the +Lambic::Storage::Account::META_PREFIX+ constant.
      def set_metadata ( metadata_headers: )
        handler.post url: url, headers: metadata_headers
      end

      # Returns the metadata for this +Account+ as a +Hash+.
      # Use the +Lambic::Storage::Account::META_PREFIX+ constant.
      def get_metadata
        handler.head(url: url).headers
      end

      # Returns an +Array+ with the name of each +Container+ for this +Account+.
      def list_containers
        handler.get(url: url).body.split(/\n/)
      end

      # Returns +true+ if a +Container+ with +container_name+ exists for this
      # account.
      def has? ( container_name: )
        list_containers.include? container_name
      end

      # Returns the +Container+ with +container_name+ if it exists for this
      # +Account+.
      # Otherwise nil.
      def fetch ( container_name: )
        Container.new handler: handler, name: container_name \
          if has? container_name: container_name
      end

      # Returns all containers of this account as an +Array+ of +Container+
      def fetch_all
        list_containers.map { |e| fetch container_name: e }
      end

      private def handler
        @handler
      end
      
      private def url
        handler.api_url api: "v1"
      end
    end
  end
end
