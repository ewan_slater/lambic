module Lambic
  # Wrapper for the Oracle Storage Cloud service REST API
  # For more details on the Oracle Storage Cloud Service, see:
  #
  # https://cloud.oracle.com/storage
  #
  # https://docs.oracle.com/cloud/latest/storagecs_common/CSSTO/toc.htm
  module Storage
  end
end
