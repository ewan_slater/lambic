require "minitest/autorun"
require 'webmock/minitest'
require_relative "../lib/lambic"

include Lambic
include Lambic::Storage

describe (StorageObject) do
  before do
    @opc_dom = "gboracle12464"
    @opc_usr = "ann.onymous@oracle.com"
    @opc_pwd = "Pa55w0rd"
    @sh = Handler.new domain: @opc_dom, username: @opc_usr, password: @opc_pwd
    stub_request(:get, "https://" + @opc_dom + ".storage.oraclecloud.com/auth/v1.0") \
      .with(:headers => {"X-Storage-User" => "Storage-" + @opc_dom + ":" + @opc_usr, 'X-Storage-Pass' => @opc_pwd}) \
      .to_return(:status => 200, :body => '', :headers => {"X-Auth-Token" => "AUTH_tk96ac34f04e6b8c70766280dd313a0d5"})

    @vwfe = StorageObject.new handler: @sh, container_name: "ruby-horse", name: "vw-fire-engine.jpg"
    @audi = StorageObject.new handler: @sh, container_name: "four-sprung-duck-technique", name: "audi.jpg"
    stub_request(:get, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464") \
      .to_return(:status => 200, :body => "ruby-horse\nruby-pony\n")
    stub_request(:get, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464/ruby-horse")\
      .to_return(:status => 200, :body => "flying-boat.JPG\nvw-fire-engine.jpg\n")
  end
  describe "#exists?" do
    before do
      @volvo = StorageObject.new handler: @sh, container_name: "ruby-horse", name: "volvo.jpg"
    end
    describe "object exists" do
      it "returns true" do
        @vwfe.exists?.must_equal true
      end
    end
    describe "object does not exist" do
      describe "parent container does not exist" do
        before do
          stub_request(:get, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464/four-sprung-duck-technique")\
            .to_return(:status => 404)
        end
        it "returns false" do
          @audi.exists?.must_equal false
        end
      end
      describe "parent container exist, object doesn't" do
        it "returns false" do
          @volvo.exists?.must_equal false
        end
      end
    end
  end
  describe "#create" do
    before do
      @src_file = "test/storage_storageobject_spec.rb"
      @beach = StorageObject.new handler: @sh, container_name: "ruby-horse", name: "beach"
    end
    describe "object created successfully" do
      before do
        stub_request(:put, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464/ruby-horse/beach") \
          .to_return(:status => 201)
      end
      it "returns with status code 201" do
        @beach.create(filename: @src_file).status_code.must_equal 201
      end
    end
    describe "request failed" do
      before do
        stub_request(:put, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464/ruby-horse/beach") \
          .to_return(:status => 418)
      end
      it "raises RESTError" do
        lambda { @beach.create(filename: @src_file) }.must_raise RESTError
      end
    end
  end
  describe "#delete" do
    describe "successful delete" do
      before do
        stub_request(:delete, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464/ruby-horse/vw-fire-engine.jpg")\
          .to_return(:status => 204)
      end
      it "returns with status code 204" do
        @vwfe.delete.status_code.must_equal 204
      end
    end
    describe "unsuccessful delete" do
      before do
        stub_request(:delete, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464/four-sprung-duck-technique/audi.jpg")\
          .to_return(:status => 404)
      end
      it "raises RESTError" do
        lambda { @audi.delete }.must_raise RESTError
      end
    end
  end
  describe "#copy" do
    before do
      @source = StorageObject.new handler: @sh, container_name: "ruby-horse", name: "flying-boat.JPG"
      @target = StorageObject.new handler: @sh, container_name: "ruby-pony", name: "flying-boat-2.JPG"
    end
    describe "succeeds with only container_name specified" do
      before do
        stub_request(:put, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464/ruby-horse/flying-boat.JPG") \
          .with(:headers => {"Destination" => "/ruby-pony/flying-boat.JPG"} )
          .to_return(:status => 201)
      end
      it "returns with status_code 201" do
        @source.copy_to(container_name: "ruby-pony").status_code.must_equal 201
      end
    end
    describe "succeeds with both container_name and object_name specified" do
      before do
        stub_request(:put, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464/ruby-horse/flying-boat.JPG") \
          .with(:headers => {"Destination" => "/ruby-pony/flying-boat-2.JPG"} )
          .to_return(:status => 201)
      end
      it "returns with status_code 201" do
        @source.copy_to(container_name: "ruby-pony", object_name: "flying-boat-2.JPG").status_code.must_equal 201
      end
    end
    describe "failure" do
      before do
        stub_request(:put, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464/ruby-horse/flying-boat.JPG") \
          .to_return(:status => 418)
      end
      it "raises RESTError" do
        lambda { @source.copy_to(container_name: "ruby-pony") }.must_raise RESTError
      end
    end
  end
  describe "#download_to" do
    describe "successful download" do
      before do
        @dest_filename = "test/cow.jpg"
        stub_request(:get, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464") \
          .to_return(:status => 200, :body => "ruby-horse\nruby-pony\n")
        stub_request(:get, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464/ruby-horse/vw-fire-engine.jpg") \
          .to_return(:status => 200)
      end
      it "returns nil" do
        @vwfe.download_to(filename: @dest_filename).must_be_nil
      end
      after do
        File.delete(@dest_filename) if File.exist?(@dest_filename)
      end
    end
  end
  describe "#update_metadata" do
    before do
      @meta = { "X-Object-Meta-Factor" => "Simon Cowell" }
      stub_request(:post, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464/ruby-horse/vw-fire-engine.jpg")\
        .with(:headers => {"X-Object-Meta-Factor" => "Simon Cowell"})
        .to_return(:status => 202)
      stub_request(:post, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464/four-sprung-duck-technique/audi.jpg")\
        .with(:headers => {"X-Object-Meta-Factor" => "Simon Cowell"})
        .to_return(:status => 404)
    end
    describe "successful update" do
      it "returns status_code 202" do
        @vwfe.set_metadata(metadata_headers: @meta).status_code.must_equal 202
      end
    end
    describe "failure" do
      it "raises RESTError" do
        lambda { @audi.set_metadata(metadata_headers: @meta)}.must_raise RESTError
      end
    end
  end
  describe "#get_metadata" do
    before do
      stub_request(:head, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464/ruby-horse/vw-fire-engine.jpg") \
        .to_return(:status => 200, :body => '', :headers => {"X-Object-Meta-Factor" => "Simon Cowell"})
    end
    it "gets the Object metadata" do
      @vwfe.get_metadata.must_include "X-Object-Meta-Factor"
    end
  end
  describe "#restore_from_archive" do
    before do
      stub_request(:post, "https://gboracle12464.storage.oraclecloud.com/v0/Storage-gboracle12464/ruby-horse/vw-fire-engine.jpg?restore")\
        .to_return(:status => 202, :headers => {"X-Archive-Restore-Tracking" => \
          "https://storage.us2.oraclecloud.com/v0/Storage-gboracle12464/arc?jobs&jobId=c8053a9fcefcfe338fe48ba3ce0113e5301c4f95", \
          "X-Archive-Restore-JobId" => "c8053a9fcefcfe338fe48ba3ce0113e5301c4f95"})
    end
    it "returns the tracking url" do
      @vwfe.restore_from_archive.must_equal \
        "https://storage.us2.oraclecloud.com/v0/Storage-gboracle12464/arc?jobs&jobId=c8053a9fcefcfe338fe48ba3ce0113e5301c4f95"
    end
  end
  describe "#track_restore" do
    before do
      stub_request(:get, \
        "https://storage.us2.oraclecloud.com/v0/Storage-gboracle12464/arc?jobs&jobId=c8053a9fcefcfe338fe48ba3ce0113e5301c4f95") \
        .to_return(:status => 200)
    end
    it "returns status_code 200" do
      trk_url = "https://storage.us2.oraclecloud.com/v0/Storage-gboracle12464/arc?jobs&jobId=c8053a9fcefcfe338fe48ba3ce0113e5301c4f95"
      @vwfe.track_restore(track_url: trk_url).status_code.must_equal 200
    end
  end
end
