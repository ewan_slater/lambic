require "minitest/autorun"
require 'webmock/minitest'
require_relative "../lib/lambic"

include Lambic
include Lambic::Storage

describe (Account) do
  before do
    @opc_dom = "gboracle12464"
    @opc_usr = "ann.onymous@oracle.com"
    @opc_pwd = "Pa55w0rd"
    @sh = Handler.new domain: @opc_dom, username: @opc_usr, password: @opc_pwd
    @sa = Account.new handler: @sh
    stub_request(:get, "https://" + @opc_dom + ".storage.oraclecloud.com/auth/v1.0") \
      .with(:headers => {"X-Storage-User" => "Storage-" + @opc_dom + ":" + @opc_usr, 'X-Storage-Pass' => @opc_pwd}) \
      .to_return(:status => 200, :body => '', :headers => {"X-Auth-Token" => "AUTH_tk96ac34f04e6b8c70766280dd313a0d5"})
  end

  describe "#set_metadata" do
    before do
      stub_request(:post, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464")\
        .with(:headers => {"X-Account-Meta-Bank" => "Meta-Barclays"}).to_return(:status => 204, :body => "")
    end
    it "sets the metadata on the Account and returns status code 204" do
      @sa.set_metadata(metadata_headers: {"X-Account-Meta-Bank" => "Meta-Barclays"})
        .status_code.must_equal 204
    end
  end

  describe "#get_metadata" do
    before do
      stub_request(:head, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464") \
        .to_return(:status => 204, :body => '', :headers => { "Server" => "Oracle-Storage-Cloud-Service", "X-Account-Container-Count" => 7, \
          "X-Account-Meta-Policy-Georeplication" => "us2", "X-Account-Object-Count" => 7965, "X-Timestamp" => 1417113600.85843, \
          "X-Account-Meta-Pi" => 3.1459726 })
    end
    it "gets the Account metadata" do
      @sa.get_metadata.must_include "Server"
      @sa.get_metadata.must_include "X-Account-Container-Count"
    end
  end

  describe "contents" do
    before do
      @containers = ["dbcs-test","jcs-test", "ruby-horse"]
      stub_request(:get, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464") \
        .to_return(:status => 200, :body => ("dbcs-test\njcs-test\nruby-horse"))
    end
    describe "#list_containers" do
      it "returns an Array of container names" do
        c_names = @sa.list_containers
        c_names.length.must_equal @containers.length
        c_names.must_be_instance_of Array
        c_names.each { |c| @containers.must_include c }
      end
    end
    describe "#has?" do
      describe "if container exists" do
        it "returns true" do
          @sa.has?(container_name: "ruby-horse").must_equal true
        end
      end
      describe "if container doesn't exist" do
        it "returns false" do
          @sa.has?(container_name: "ruby-pony").must_equal false
        end
      end
    end
    describe "#fetch" do
      describe "if container exists" do
        it "returns the container" do
          c = @sa.fetch(container_name: "ruby-horse")
          c.must_be_instance_of Container
          c.name.must_equal "ruby-horse"
        end
      end
      describe "if container doesn't exist" do
        it "returns nil" do
          @sa.fetch(container_name: "ruby-pony").must_equal nil
        end
      end
    end
    describe "#fetch_all" do
      it "returns an Array of containers" do
        contents = @sa.fetch_all
        contents.must_be_instance_of Array
        contents.length.must_equal @containers.length
        contents.each { |c| @containers.must_include c.name }
      end
    end
  end
end
