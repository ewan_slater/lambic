require "minitest/autorun"
require 'webmock/minitest'
require_relative "../lib/lambic"

include Lambic
include Lambic::Storage

describe (Container) do
  before do
    ENV["LAMBIC_STORAGE_API_URL_V1"] = "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464"
    @opc_dom = "gboracle12464"
    @opc_usr = "ann.onymous@oracle.com"
    @opc_pwd = "Pa55w0rd"
    stub_request(:get, "https://" + @opc_dom + ".storage.oraclecloud.com/auth/v1.0") \
      .with(:headers => {"X-Storage-User" => "Storage-" + @opc_dom + ":" + @opc_usr, 'X-Storage-Pass' => @opc_pwd}) \
      .to_return(:status => 200, :body => '', :headers => {"X-Auth-Token" => "AUTH_tk96ac34f04e6b8c70766280dd313a0d5"})
  end
  describe "#exists?" do
    before do
      sh = Handler.new domain: @opc_dom, username: @opc_usr, password: @opc_pwd
      @rh = Container.new handler: sh, name: "ruby-horse"
      @rp = Container.new handler: sh, name: "ruby-pony"
      stub_request(:get, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464") \
        .to_return(:status => 200, :body => ("dbcs-test\njcs-test\nruby-horse"))
    end
    describe "if container does exist" do
      it "returns true" do
        @rh.exists?.must_equal true
      end
    end
    describe "if container does not exist" do
      it "returns false" do
        @rp.exists?.must_equal false
      end
    end
  end

  describe "#create" do
    before do
      sh = Handler.new domain: @opc_dom, username: @opc_usr, password: @opc_pwd
      @rh = Container.new handler: sh, name: "ruby-horse"
      @rp = Container.new handler: sh, name: "ruby-pony"
      stub_request(:get, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464") \
        .to_return(:status => 200, :body => ("dbcs-test\njcs-test\nruby-horse"))
      stub_request(:put, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464/ruby-pony") \
        .to_return(:status => 201)
    end
    describe "creates the container with standard storage class" do
      describe "if the container does not exist" do
        it "is created and status code 201 returned" do
          @rp.create.status_code.must_equal 201
        end
      end
      describe "if the container already exists" do
        it "has no effect and returns nil" do
          @rh.create.must_equal nil
        end
      end
    end
  end

  describe "#create_as_archive" do
    before do
      sh = Handler.new domain: @opc_dom, username: @opc_usr, password: @opc_pwd
      @rh = Container.new handler: sh, name: "ruby-horse"
      @rp = Container.new handler: sh, name: "ruby-pony"
      stub_request(:get, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464") \
        .to_return(:status => 200, :body => ("dbcs-test\njcs-test\nruby-horse"))
      stub_request(:put, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464/ruby-pony") \
        .with(:headers => {"X-Storage-Class" => "Archive"})
        .to_return(:status => 201)
    end
    describe "creates the container with archive storage class" do
      describe "if the container does not exist" do
        it "is created and status code 201 returned" do
          @rp.create_as_archive.status_code.must_equal 201
        end
      end
      describe "if the container already exists" do
        it "has no effect and returns nil" do
          @rh.create_as_archive.must_equal nil
        end
      end
    end
  end
  describe "container contents" do
    before do
      @sh = Handler.new domain: @opc_dom, username: @opc_usr, password: @opc_pwd
      @rp = Container.new handler: @sh, name: "ruby-pony"
      stub_request(:get, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464/ruby-pony")\
        .to_return(:status => 204, :body => "")

      @rh = Container.new handler: @sh, name: "ruby-horse"
      stub_request(:get, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464/ruby-horse")\
        .to_return(:status => 200, :body => "flying-boat.JPG\nvw-fire-engine.jpg\n")
    end
    describe "#list_objects" do
      describe "for an empty container" do
        it "returns an empty array" do
          r = @rp.list_objects
          r.must_be_instance_of Array
          r.must_be_empty
        end
      end
      describe "for a non empty container" do
        it "returns an array of all the objects it contains" do
          r = @rh.list_objects
          r.must_be_instance_of Array
          r.wont_be_empty
          r.length.must_equal 2
        end
      end
      describe "for a non empty container with a filter applied" do
        before do
          stub_request(:get, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464/ruby-horse?limit=1")\
            .to_return(:status => 200, :body => "flying-boat.JPG\n")
        end
        it "returns a filtered Array of objects" do
          r = @rh.list_objects filter_by: "limit", filter_value: 1
          r.must_be_instance_of Array
          r.wont_be_empty
          r.length.must_equal 1
        end
      end
    end
    describe "#has?" do
      before do
        @q = lambda { | box | box.has?(object_name: "flying-boat.JPG") }
      end
      describe "if the object is in the container" do
        it "returns true" do
          @q.call(@rh).must_equal true
        end
      end
      describe "if the object isn't in the container" do
        it "returns false" do
          @q.call(@rp).must_equal false
        end
      end
    end
    describe "#empty?" do
      describe "for a non empty container" do
        it "returns false" do
          @rh.empty?.must_equal false
        end
      end
      describe "for an empty container" do
        it "returns true" do
          @rp.empty?.must_equal true
        end
      end
    end
    describe "#fetch" do
      describe "object is in the container" do
        it "returns the object" do
          o = @rh.fetch object_name: "flying-boat.JPG"
          o.must_be_instance_of StorageObject
          o.name.must_equal "flying-boat.JPG"
        end
      end
      describe "object isn't in the container" do
        it "returns nil" do
          @rp.fetch(object_name: "flying-boat.JPG").must_be_nil
        end
      end
    end
    describe "#fetch_all" do
      describe "empty container" do
        it "returns an empty array" do
          @rp.fetch_all.must_be_instance_of Array
          @rp.fetch_all.must_be_empty
        end
      end
      describe "non empty container" do
        it "returns an array of all objects in the container" do
          r = @rh.fetch_all
          r.must_be_instance_of Array
          r.length.must_equal 2
          names = r.map { |e| e.name }
          names.must_include "flying-boat.JPG"
          names.must_include "vw-fire-engine.jpg"
        end
      end
    end
  end

  describe "#delete_objects" do
    before do
      @sh = Handler.new domain: @opc_dom, username: @opc_usr, password: @opc_pwd
    end
    describe "for non empty container" do
      before do
        stub_request(:get, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464/ruby-horse")\
          .to_return(:status => 200, :body => "flying-boat.JPG\nvw-fire-engine.jpg\n")
        stub_request(:delete, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464/ruby-horse/flying-boat.JPG")\
          .to_return(:status => 204)
        stub_request(:delete, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464/ruby-horse/vw-fire-engine.jpg")\
          .to_return(:status => 204)
        @rh = Container.new handler: @sh, name: "ruby-horse"
      end
      it "deletes the objects and returns an Array of the deleted object names" do
        r = @rh.delete_objects
        r.wont_be_empty
        r.must_be_instance_of Array
        r.length.must_equal 2
        r.must_include "flying-boat.JPG"
        r.must_include "vw-fire-engine.jpg"
      end
    end
    describe "for an empty container" do
      before do
        stub_request(:get, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464/ruby-pony")\
          .to_return(:status => 204, :body => "")
        @rp = Container.new handler: @sh, name: "ruby-pony"
      end
      it "returns an empty Array" do
        r = @rp.delete_objects
        r.must_be_empty
        r.must_be_instance_of Array
      end
    end
  end

  describe "#delete" do
    before do
      @sh = Handler.new domain: @opc_dom, username: @opc_usr, password: @opc_pwd
    end
    describe "if container is empty" do
      before do
        stub_request(:get, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464/ruby-pony")\
          .to_return(:status => 204, :body => "")
        stub_request(:delete, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464/ruby-pony")\
          .to_return(:status => 204)
        @rp = Container.new handler: @sh, name: "ruby-pony"
      end
      it "deletes the container" do
        @rp.delete.status_code.must_equal 204
      end
    end
    describe "if container not empty" do
      before do
        stub_request(:get, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464/ruby-horse")\
          .to_return(:status => 200, :body => "flying-boat.JPG\nvw-fire-engine.jpg\n")
        @rh = Container.new handler: @sh, name: "ruby-horse"
      end
      it "returns nil" do
        @rh.delete.must_be_nil
      end
    end
  end

  describe "#delete!" do
    before do
      @sh = Handler.new domain: @opc_dom, username: @opc_usr, password: @opc_pwd
    end
    describe "if container is empty" do
      before do
        stub_request(:get, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464/ruby-pony")\
          .to_return(:status => 204, :body => "")
        stub_request(:delete, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464/ruby-pony")\
          .to_return(:status => 204)
        @rp = Container.new handler: @sh, name: "ruby-pony"
      end
      it "deletes the container" do
        @rp.delete!.status_code.must_equal 204
      end
    end
    describe "if container not empty" do
      before do
        stub_request(:delete, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464/ruby-horse")\
          .to_return(:status => 204, :body => "")
        stub_request(:get, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464/ruby-horse")\
          .to_return(:status => 200, :body => "flying-boat.JPG\nvw-fire-engine.jpg\n").times(1).then\
          .to_return(:status => 204)
        stub_request(:delete, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464/ruby-horse/flying-boat.JPG")\
          .to_return(:status => 204)
        stub_request(:delete, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464/ruby-horse/vw-fire-engine.jpg")\
          .to_return(:status => 204)
        @rh = Container.new handler: @sh, name: "ruby-horse"
      end
      it "deletes the objects and then the container" do
        @rh.delete!.status_code.must_equal 204
      end
    end
  end

  describe "#set_metadata" do
    before do
      @sh = Handler.new domain: @opc_dom, username: @opc_usr, password: @opc_pwd
      @rh = Container.new handler: @sh, name: "ruby-horse"
      stub_request(:post, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464/ruby-horse")\
        .with(:headers => {"X-Container-Meta-Fowl" => "Meta-Chicken"}).to_return(:status => 204, :body => "")
    end
    it "sets the metadata on the container and returns status code 204" do
      @rh.set_metadata(metadata_headers: {"X-Container-Meta-Fowl" => "Meta-Chicken"})
        .status_code.must_equal 204
    end
  end

  describe "#get_metadata" do
    before do
      @sh = Handler.new domain: @opc_dom, username: @opc_usr, password: @opc_pwd
      @rh = Container.new handler: @sh, name: "ruby-horse"
      stub_request(:head, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464/ruby-horse") \
        .to_return(:status => 204, :body => '', :headers => { "Server" => "Oracle-Storage-Cloud-Service", "X-Container-Object-Count" => 2 })
    end
    it "gets the container metadata" do
      @rh.get_metadata.must_include "Server"
      @rh.get_metadata.must_include "X-Container-Object-Count"
    end
  end
end
