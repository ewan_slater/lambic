require "minitest/autorun"
require 'webmock/minitest'
require_relative "../lib/lambic"

include Lambic
include Lambic::Storage

describe (Handler) do
  before do
    @opc_dom = "gboracle12464"
    @opc_usr = "ann.onymous@oracle.com"
    @opc_pwd = "Pa55w0rd"
    stub_request(:get, "https://" + @opc_dom + ".storage.oraclecloud.com/auth/v1.0").to_return(:status => 401)
    stub_request(:get, "https://" + @opc_dom + ".storage.oraclecloud.com/auth/v1.0") \
      .with(:headers => {"X-Storage-User" => "Storage-" + @opc_dom + ":" + @opc_usr, 'X-Storage-Pass' => @opc_pwd}) \
      .to_return(:status => 200, :body => '', :headers => {"X-Auth-Token" => "AUTH_tk96ac34f04e6b8c70766280dd313a0d5"})
  end

  describe "#get_auth_token" do
    # testing a private method because if this doesn't work
    # then nothing else will either
    before do
      @right = Handler.new domain: @opc_dom, username: @opc_usr, password: @opc_pwd
      @wrong = Handler.new domain: @opc_dom, username: @opc_usr + "wrong", password: @opc_pwd + "wrong"
    end
    describe "good credentials" do
      it "gets an Auth header" do
        r = @right.send(:get_auth_token)
        r.must_equal "AUTH_tk96ac34f04e6b8c70766280dd313a0d5"
      end
    end
    describe "bad credentials" do
      it "it raises RESTError" do
        lambda { @wrong.send(:get_auth_token) }.must_raise RESTError
      end
    end
  end

  describe "#get" do
    before do
      @sh = Handler.new domain: @opc_dom, username: @opc_usr, password: @opc_pwd
      stub_request(:get, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464") \
        .to_return(:status => 200, :body => "dbcs-test\njcs-test\n")
      stub_request(:get, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle124644") \
        .to_return(:status => 404)
    end
    describe "good request" do
      it "returns an HTTP::Message containing the body contents and status code 200" do
        @sh.get(url: @sh.api_url(api: "v1")).status_code.must_equal 200
      end
    end
    describe "bad request" do
      it "raises RESTError" do
        lambda { @sh.get(url: @sh.api_url(api: "v1") + "4") }.must_raise RESTError
      end
    end
  end

  describe "#head" do
    before do
      @sh = Handler.new domain: @opc_dom, username: @opc_usr, password: @opc_pwd
      stub_request(:head, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464") \
        .to_return(:status => 204, :body => '', :headers => { "Server" => "Oracle-Storage-Cloud-Service", "X-Account-Container-Count" => 7, \
            "X-Account-Meta-Policy-Georeplication" => "us2", "X-Account-Object-Count" => 7965, "X-Timestamp" => 1417113600.85843, \
            "X-Account-Meta-Pi" => 3.1459726 })
      stub_request(:head, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle124644") \
        .to_return(:status => 404)
    end
    describe "good request" do
      it "returns an HTTP::Message, with status code 204" do
        @sh.head(url: @sh.api_url(api: "v1")).status_code.must_equal 204
      end
    end
    describe "bad request" do
      it "raises RESTError" do
        lambda { @sh.head(url: @sh.api_url(api: "v1") + "4") }.must_raise RESTError
      end
    end
  end

  describe "#put" do
    before do
      @sh = Handler.new domain: @opc_dom, username: @opc_usr, password: @opc_pwd
      stub_request(:put, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464") \
        .to_return(:status => 201, :body => '', :headers => { "Server" => "Oracle-Storage-Cloud-Service", \
            "X-Trans-Id" => "txb5d8fad18ab04fa99ecc0-0056000a2cga" })
      stub_request(:put, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle124644") \
        .to_return(:status => 404)
    end
    describe "good request" do
      it "returns an HTTP::Message, with status code 201" do
        @sh.put(url: @sh.api_url(api: "v1")).status_code.must_equal 201
      end
    end
    describe "bad request" do
      it "raises RESTError" do
        lambda { @sh.put(url: @sh.api_url(api: "v1") + "4") }.must_raise RESTError
      end
    end
  end

  describe "#delete" do
    before do
      @sh = Handler.new domain: @opc_dom, username: @opc_usr, password: @opc_pwd
      stub_request(:delete, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464/beccles") \
        .to_return(:status => 204, :body => '', :headers => { "Server" => "Oracle-Storage-Cloud-Service", \
            "X-Trans-Id" => "tx33b93259a60443ecb616c-0056000a2bga" })
      stub_request(:delete, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464/bungay") \
        .to_return(:status => 404)
    end
    describe "good request" do
      it "returns an HTTP::Message, with status code 204" do
        @sh.delete(url: @sh.api_url(api: "v1") + "/beccles").status_code.must_equal 204
      end
    end
    describe "bad request" do
      it "raises RESTError" do
        lambda { @sh.delete(url: @sh.api_url(api: "v1") + "/bungay") }.must_raise RESTError
      end
    end
  end

  describe "#post" do
    before do
      @sh = Handler.new domain: @opc_dom, username: @opc_usr, password: @opc_pwd
      stub_request(:post, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle12464") \
        .with(:headers => {"X-Account-Meta-Rodent" => "Stainless Steel Rat"})
        .to_return(:status => 204, :body => '', :headers => {"X-Account-Meta-Rodent" => "Stainless Steel Rat"})
      stub_request(:post, "https://gboracle12464.storage.oraclecloud.com/v1/Storage-gboracle124644") \
        .to_return(:status => 404)
    end
    describe "good request" do
      it "returns an HTTP::Message, with status code 204 and containing the headers just set" do
        r = @sh.post url: @sh.api_url(api: "v1"), headers: {"X-Account-Meta-Rodent" => "Stainless Steel Rat"}
        r.status_code.must_equal 204
        r.headers.must_include "X-Account-Meta-Rodent"
        r.headers['X-Account-Meta-Rodent'].must_equal "Stainless Steel Rat"
      end
    end
    describe "bad request" do
      it "raises RESTError" do
        lambda { @sh.post(url: @sh.api_url(api: "v1") + "4") }.must_raise RESTError
      end
    end
  end
end
