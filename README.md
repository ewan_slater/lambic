# Lambic #

## Description ##

Lambic is a set of example Ruby bindings for Oracle Cloud Service REST APIs.

## Disclaimer ##

Lambic is *not* an Oracle product or offering.


## Usage ##

Each Oracle Cloud Service has a REST API.

The REST API for each service is wrapped in it's own module.

### Oracle Storage Cloud Service ###

To access the REST API for the Oracle Storage Cloud Service, use the
<b>Lambic::Storage</b> module.

Start by creating and instance of Lambic::Storage::Handler with the domain,
username and password.

The Handler is passed to the other objects you create and handles their
requests to the service.

Lambic::Storage::Account provides account operations.
Lambic::Storage::Container provides container operations.
Lambic::Storage::StorageObject provides object operations.

## Requirements ##

Lambic depends on the following gems:
* httpclient
* webmock
* require_all
* hoe

## Installation ##

* (sudo) gem install lambic
* Otherwise download the source code and use the files locally:

  - install required gems:
    $ gem install httpclient
    $ gem install webmock
    $ gem install require_all
    $ gem install hoe

  - to run tests:
    $ rake

  - to build:
    $ rake gem

## License ##

(The MIT License)

Copyright (c) 2015

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
'Software'), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
